#include <QDebug>

#include "appproxymanager.h"

AppProxyManager::AppProxyManager(QObject *parent) :
    QObject(parent), m_currentUI()
{
    qDebug() << "AppProxyManager constructor";
}

void AppProxyManager::startUI(const QString& canonicalID)
{
    qDebug() << "AppProxyManager::startUI" << canonicalID;
    m_currentUI = canonicalID;
    emit currentUIChanged();
}

QString AppProxyManager::currentUI()
{
    return m_currentUI;
}
