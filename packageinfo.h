#ifndef PACKAGEINFO_H
#define PACKAGEINFO_H

#include <QObject>

class PackageInfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name)
    Q_PROPERTY(QString icone READ icone)

public:
    PackageInfo(const QString& name, const QString& icone);

    QString name();
    QString icone();

private:
    QString m_name;
    QString m_icone;
};

#endif // PACKAGEINFO_H
