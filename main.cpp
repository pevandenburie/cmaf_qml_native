#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer.h"

#include <QQmlEngine>
#include <QQmlContext>
#include <QQmlComponent>

#include <QStringList>

#include <packageinfo.h>
#include <appproxymanager.h>


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQuickView view;
    QQmlContext* context = view.engine()->rootContext();

    AppProxyManager appProxyManager;


    // You will understand this line in a few slides, don't worry now!
    // It is needed so that we can reference the Gender enum.
    //qmlRegisterType<User>( "UserData", 1, 0, "User");

    QList<QObject*> packagesList;
    packagesList.append(new PackageInfo(QString("Applications"), QString("pics/home.png")));
    packagesList.append(new PackageInfo(QString("BSA"), QString("pics/store.png")));
    packagesList.append(new PackageInfo(QString("Package Manager"), QString("pics/appstore.jpeg")));
    packagesList.append(new PackageInfo(QString("Running..."), QString("pics/defaultIcon.jpeg")));


    context->setContextProperty("cmaf_packagesList", QVariant::fromValue(packagesList));
    context->setContextProperty("cmaf_appProxyManager", &appProxyManager);

    view.setSource(QUrl("qml/cmaf_qml_native/CmafWelcomeGrid.qml"));
    view.show();
    return app.exec();
}
