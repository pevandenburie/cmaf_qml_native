import QtQuick 2.0

Rectangle {
    width: 400
    height: 300


    GridView {
        id: gridView
        anchors.fill: parent
        cellWidth: 160; cellHeight: 80
        clip: true

        header: Rectangle {
            width: parent.width; height: 30
            color: "grey"
            anchors.horizontalCenter: parent.horizontalCenter
            Text { text: "CMAF"; anchors.centerIn: parent }
        }

        highlight: Rectangle {
            width: gridView.cellWidth; height: gridView.cellHeight
            color: "lightsteelblue"; radius: 5

//            MouseArea {
//                anchors.fill: parent
//                onClicked: console.log(  /*appText.text +*/ " mouse pressed")
//            }
            /*Keys.onPressed: {
                console.log( event.key + " pressed")
            }*/ // does not work
        }
        focus: true

        //model: CmafAppModel {}
        model: cmaf_packagesList
        delegate: CmafAppDelegate {}

        add: Transition {
            NumberAnimation {
                properties: "x,y"; from: 100; duration: 1000
            }
        }

//        Keys.onPressed: {
//            if ((event.key == Qt.Key_Enter)
//                    || (event.key == Qt.Key_Return)
//                    || (event.key == Qt.Key_Select)){
//                console.log( " pressed")
//                GridView.currentItem.
//            }
//        }
    }
}
