import QtQuick 2.0


ListModel {

    ListElement {
        name: "Applications"
        icone: "pics/home.png"
    }
    ListElement {
        name: "BSA"
        icone: "pics/store.png"
    }
    ListElement {
        name: "Package Manager"
        icone: "pics/appstore.jpeg"
    }
    ListElement {
        name: "Running..."
        icone: "pics/defaultIcon.jpeg"
    }
}
