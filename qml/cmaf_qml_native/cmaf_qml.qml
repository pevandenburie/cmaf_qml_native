import QtQuick 2.0

Item {
    id: container
    width: 400
    height: 300

    Rectangle {
        id: welcomeScreen
        width: parent.width
        height: parent.height
        gradient: Gradient {
            GradientStop { position: 0.0; color: "light grey" }
            GradientStop { position: 1.0; color: "grey" }
        }

        Text {
            id: welcomeTitle
            height: 50
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top
            }
            text: "Welcome into CMAF"
            color: "steel blue"
        }

        CmafButton {
            id: appsMenuButton
            focus: true
            anchors { top: welcomeTitle.bottom }
            text: "Apps Menu"
            Keys.onPressed: {
                if (event.key == Qt.Key_Space) console.log( text + " pressed")
                else if (event.key == Qt.Key_Up) installAppsButton.focus = true
                else if (event.key == Qt.Key_Down) runningAppsButton.focus = true
            }
        }

        CmafButton {
            id: runningAppsButton
            anchors { top: appsMenuButton.bottom }
            text: "Running Apps"
            Keys.onPressed: {
                if (event.key == Qt.Key_Space) console.log( text + " pressed")
                else if (event.key == Qt.Key_Up) appsMenuButton.focus = true
                else if (event.key == Qt.Key_Down) installAppsButton.focus = true
            }
        }

        CmafButton {
            id: installAppsButton
            anchors { top: runningAppsButton.bottom }
            text: "Install Apps"
            Keys.onPressed: {
                if (event.key == Qt.Key_Space) console.log( text + " pressed")
                else if (event.key == Qt.Key_Up) runningAppsButton.focus = true
                else if (event.key == Qt.Key_Down) appsMenuButton.focus = true
            }
        }
    }
}



