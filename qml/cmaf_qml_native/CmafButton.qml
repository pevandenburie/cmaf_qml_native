import QtQuick 2.0


Rectangle {
    id: cmafButton
    height: 50
    color: focus ? "red" : "steel blue"

    property alias text: cmafButtonText.text

    anchors {
        top: parent.top
        left: parent.left
        right: parent.right
        margins: 20
    }
    Text { id:cmafButtonText; text: "cmafButton"; color: "black"; anchors.centerIn: parent }

    /*Keys.onPressed: {
        if (event.key == Qt.Key_Space) console.log( appsMenuText.text + " pressed")
        else if (event.key == Qt.Key_Up) installAppsButton.focus = true
        else if (event.key == Qt.Key_Down) runningAppsButton.focus = true
    }*/
}


