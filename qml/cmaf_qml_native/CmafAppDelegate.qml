import QtQuick 2.0

//Component {
    //id: cmafAppDelegate

    /*property alias width: wrapper.width
    property alias height: wrapper.height*/

    Item {
        id: wrapper
        width: 160; height: 80

        Column {
            anchors.fill: parent
            Image {
                id: appIcone
                source: icone;
                anchors.horizontalCenter: parent.horizontalCenter
                //width: 60; height: 60
                height: wrapper.height - appText.height ; width: height
            }
            Text {
                id: appText
                text: name;
                height: 20
                //anchors.top: appIcone.bottom  //QML Column: Cannot specify top, bottom, verticalCenter, fill or centerIn anchors for items inside Column. Column will not function.
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                //console.log( appText.text + " pressed");
            }
        }

        Keys.onPressed: {
            if (event.key == Qt.Key_Return) {
                console.log( appText.text + " pressed")
                cmaf_appProxyManager.startUI(appText.text);
            }
        }
    }
//}
