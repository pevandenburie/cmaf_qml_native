#ifndef APPPROXYMANAGER_H
#define APPPROXYMANAGER_H

#include <QObject>
#include "packageinfo.h"

class AppProxyManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString currentUI READ currentUI WRITE startUI NOTIFY currentUIChanged)

public:
    explicit AppProxyManager(QObject *parent = 0);

    Q_INVOKABLE void startUI(const QString& canonicalID);
    Q_INVOKABLE QString currentUI();

signals:
    void currentUIChanged();

public slots:

private:
    QString m_currentUI;

};

#endif // APPPROXYMANAGER_H
