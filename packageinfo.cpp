#include "packageinfo.h"

PackageInfo::PackageInfo(const QString& name, const QString& icone)
    : QObject(), m_name(name), m_icone(icone) {
}

QString PackageInfo::name() { return m_name; }
QString PackageInfo::icone() { return m_icone; }

